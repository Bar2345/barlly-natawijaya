<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class petugas extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('petugas_model');
	}

	public function index()
	{
		$this->load->view('_partials/navbar');
		$data ['query']= $this->petugas_model->read();
		$this->load->view('petugas/index',$data);
	}
	public function edit($id_petugas)
	{
		$data['query'] = $this->petugas_model->read_by_petugas($id_petugas);
		$this->load->view('petugas/edit', $data);
	}
	public function update()
	{
		$username = $this->input->post('usernmame');
		$password = $this->input->post('password');
		$nama_petugas = $this->input->post('nama_petugas');
		$level = $this->input->post('level');

		echo $username . "." . $password;

		$data['query'] = $this->petugas_model->update();

		redirect('petugas');
	}
	public function add()
	{
		$this->load->view('petugas/add');
	}
	public function insert()
	{
		
		$username = $this->input->post('usernmame');
		$password = $this->input->post('password');
		$nama_petugas = $this->input->post('nama_petugas');
		$level = $this->input->post('level');

		$data['query'] = $this->petugas_model->create();

		redirect('petugas');
	}
	public function delete($petugas)
	{
		$this->petugas_model->delete($petugas);
		redirect('petugas');
	}
}


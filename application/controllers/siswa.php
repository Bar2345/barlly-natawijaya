<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Siswa extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('siswa_model');
	}

	public function index()
	{
		$this->load->view('_partials/navbar');
		$data ['query']= $this->siswa_model->read();
		$this->load->view('siswa/index',$data);
	}
	public function edit($nisn)
	{
		$data['query'] = $this->siswa_model->read_by_nisn($nisn);
		$this->load->view('siswa/edit', $data);
	}
	public function update()
	{
		$nisn = $this->input->post('nisn');
		$nis = $this->input->post('nis');
		$nama = $this->input->post('nama');
		$id_kelas = $this->input->post('id_kelas');
		$alamat = $this->input->post('alamat');
		$no_telp = $this->input->post('no_telp');
		$id_spp = $this->input->post('id_spp');
		$password = $this->input->post('password');

		echo $nisn . "." . $id_kelas;

		$data['query'] = $this->siswa_model->update();

		redirect('siswa');
	}
	public function add()
	{
		$this->load->view('siswa/add');
	}
	public function insert()
	{
		$nisn = $this->input->post('nisn');
		$nis = $this->input->post('nis');
		$nama = $this->input->post('nama');
		$id_kelas = $this->input->post('id_kelas');
		$alamat = $this->input->post('alamat');
		$no_telp = $this->input->post('no_telp');
		$id_spp = $this->input->post('id_spp');
		$password = $this->input->post('password');
		
		echo $id_kelas .".". $nama;

		$data['query'] = $this->siswa_model->create();

		redirect('siswa');
	}

	public function delete($siswa)
	{
		$this->siswa_model->delete($siswa);
		redirect('siswa');
	}
}


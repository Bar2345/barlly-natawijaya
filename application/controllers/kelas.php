<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class kelas extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('kelas_model');
	}

	public function index()
	{
		$this->load->view('_partials/navbar');
		$data ['query']= $this->kelas_model->read();
		$this->load->view('kelas/index',$data);
	}
	public function edit($id_kelas)
	{
		$data['query'] = $this->kelas_model->read_by_kelas($id_kelas);
		$this->load->view('kelas/edit', $data);
	}
	public function update()
	{
		$id_pembayaran = $this->input->post('id_kelas');
		$nama_kelas = $this->input->post('nama_kelas');
		$kompentensi_keahlian = $this->input->post('kompentensi_keahlian');

		echo $nama_kelas . "." . $kompentensi_keahlian;

		$data['query'] = $this->kelas_model->update();

		redirect('lelas');
	}
	public function add()
	{
		$this->load->view('kelas/add');
	}
	public function insert()
	{
		$kelas = $this->input->post('nama_kelas');
		$kompetensi_keahlian = $this->input->post('kompetensi_keahlian');
		
		echo $kelas .".". $kompetensi_keahlian;

		$data['query'] = $this->kelas_model->create();

		redirect('kelas');
	}
	public function delete($id_kelas)
	{
		$this->kelas_model->delete($id_kelas);
		redirect('kelas');
	}
}


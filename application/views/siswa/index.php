<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<style>
    table,
    th,
    td {
        border: 1px solid black;
    }
</style>

<body>
    <h1>Data Siswa</h1>
    <h6><a href="siswa/add/">Tambah Data Siswa</a></h6>
    <table>
        <thead>
            <tr>
                <td>nisn</td>
                <td>nis</td>
                <td>nama</td>
                <td>id_kelas</td>
                <td>alamat</td>
                <td>no_telp</td>
                <td>id_spp</td>
                <td>password</td>
                <td>aksi</td>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($query->result() as $row) { ?>
                <tr>
                    <td><?= $row->nisn ?></td>
                    <td><?= $row->nis ?></td>
                    <td><?= $row->nama ?></td>
                    <td><?= $row->id_kelas ?></td>
                    <td><?= $row->alamat ?></td>
                    <td><?= $row->no_telp ?></td>
                    <td><?= $row->id_spp ?></td>
                    <td><?= $row->password ?></td>
                    <td>
                        <a href="<?= base_url('siswa/edit/') . $row->nisn ?>">Edit</a>
                        <a href="<?= base_url('siswa/delete/') . $row->nisn ?>">Delete</a>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</body>

</html>
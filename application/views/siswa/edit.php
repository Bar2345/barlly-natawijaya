<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h1>Edit Siswa</h1>
    <?php $row = $query->result();  ?>
    <form action="<?= base_url('siswa/update/') ?>" method="post">

        <input type="hidden" name="nisn" id="nisn" value="<?= $row[0]->nisn ?>">
        <label for="nis">nis</label>
        <input type="number" name="nis" id="nis" value="<?= $row[0]->nis ?>">
        <label for="nama">nama</label>
        <input type="text" name="nama" id="nama" value="<?= $row[0]->nama ?>">
        <label for="id_kelas">id_kelas</label>
        <input type="number" name="id_kelas" id="id_kelas" value="<?= $row[0]->id_kelas ?>">
        <label for="alamat">alamat</label>
        <input type="text" name="alamat" id="alamat" value="<?= $row[0]->alamat ?>">
        <label for="no_telp">no_telp</label>
        <input type="number" name="no_telp" id="no_telp" value="<?= $row[0]->no_telp ?>">
        <label for="id_spp">id_spp</label>
        <input type="number" name="id_spp" id="id_spp" value="<?= $row[0]->id_spp ?>">
        <label for="password">password</label>
        <input type="text" name="password" id="password" value="<?= $row[0]->password ?>">

        <input type="submit" value="Sunting">
    </form>
</body>

</html
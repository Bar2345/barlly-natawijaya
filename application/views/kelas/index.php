<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<style>
    table,
    th,
    td {
        border: 1px solid black;
    }
</style>

<body>
    <h1>Data Kelas</h1>
    <h6><a href="kelas/add/">Tambah Data Kelas</a></h6>
    <table>
        <thead>
            <tr>
                <td>ID Kelas</td>
                <td>Nama_Kelas</td>
                <td>kompetensi_keahlian</td>
                <td>Aksi</td>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($query->result() as $row) { ?>
                <tr>
                    <td><?= $row->id_kelas ?></td>
                    <td><?= $row->nama_kelas ?></td>
                    <td><?= $row->kompetensi_keahlian ?></td>
                    <td>
                        <a href="<?= base_url('kelas/edit/') . $row->id_kelas ?>">Edit</a>
                        <a href="<?= base_url('kelas/delete/') . $row->id_kelas ?>">Delete</a>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</body>

</html>
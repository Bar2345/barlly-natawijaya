<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h1>Tambah Kelas Baru</h1>

    <form action="<?= base_url('kelas/insert') ?>" method="post">
        <label for="nama_kelas">nama_kelas</label>
        <input type="text" name="nama_kelas" id="nama_kelas">
        <label for="kompetensi_keahlian">kompetensi_keahlian</label>
        <input type="text" name="kompetensi_keahlian" id="kompetensi_keahlian">

        <input type="submit" value="Tambah Kelas">
    </form>
</body>

</html>
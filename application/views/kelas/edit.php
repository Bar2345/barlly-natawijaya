<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h1>Edit Pembayaran</h1>
    <?php $row = $query->result();  ?>
    <form action="<?= base_url('kelas/update/') ?>" method="post">

        <input type="hidden" name="id_kelas" id="id_kelas" value="<?= $row[0]->id_kelas ?>">
        <label for="nama_kelas">nama_kelas</label>
        <input type="text" name="nama_kelas" id="nama_kelas" value="<?= $row[0]->nama_kelas ?>">
        <label for="kompentensi_keahlian">kompentensi_keahlian</label>
        <input type="text" name="kompentensi_keahlian" id="kompentensi_keahlian" value="<?= $row[0]->kompentensi_keahlian ?>">
        <input type="submit" value="Sunting">
    </form>
</body>

</html
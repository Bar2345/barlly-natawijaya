<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="styleshet" type="text/css" href="<?= base_url('assets/css') ?>">
</head>
<style>
    table,
    th,
    td {
        border: 1px solid black;
    }
</style>
<body>

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">Navbar</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="#">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Features</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Pricing</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link disabled">Disabled</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <h1>Data Spp</h1>
    <h6><a href="spp/add/">Tambah Data SPP</a></h6>

    <table>
        <thead>
            <tr>
                <td>id_spp</td>
                <td>Tahun</td>
                <td>Nominal</td>
                <td>Aksi</td>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($query->result() as $row) { ?>
                <tr>
                    <td><?= $row->id_spp ?></td>
                    <td><?= $row->tahun ?></td>
                    <td><?= $row->nominal ?></td>
                    <td>
                        <a href="<?= base_url('spp/edit/') . $row->id_spp ?>">Edit</a>
                        <a href="<?= base_url('spp/delete/') . $row->id_spp ?>">Delete</a>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
    <script type="text/javascript" src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
</body>

</html>
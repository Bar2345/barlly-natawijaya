<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<style>
    table,
    th,
    td {
        border: 1px solid black;
    }
</style>

<body>
    <h1>Data Petugas</h1>
    <h6><a href="petugas/add/">Tambah Data Petugas</a></h6>
    <table>
        <thead>
            <tr>
                <td>ID Petugas</td>
                <td>username</td>
                <td>password</td>
                <td>nama_petugas</td>
                <td>level</td>
                <td>aksi</td>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($query->result() as $row) { ?>
                <tr>
                    <td><?= $row->id_petugas ?></td>
                    <td><?= $row->username ?></td>
                    <td><?= $row->password ?></td>
                    <td><?= $row->nama_petugas ?></td>
                    <td><?= $row->level ?></td>
                    <td>
                        <a href="<?= base_url('petugas/edit/') . $row->id_petugas ?>">Edit</a>
                        <a href="<?= base_url('petugas/delete/') . $row->id_petugas ?>">Delete</a>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</body>

</html>
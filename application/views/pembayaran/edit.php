<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h1>Edit Pembayaran</h1>
    <?php $row = $query->result();  ?>
    <form action="<?= base_url('pembayaran/update/') ?>" method="post">

        <input type="hidden" name="id_pembayaran" id="id_pembayaran" value="<?= $row[0]->id_pembayaran ?>">
        <label for="id_petugas">id_petugas</label>
        <input type="number" name="id_petugas" id="id_petugas" value="<?= $row[0]->id_petugas ?>">
        <label for="nisn">nisn</label>
        <input type="number" name="nisn" id="nisn" value="<?= $row[0]->nisn ?>">
        <label for="tgl_bayar">tgl_bayar</label>
        <input type="text" name="tgl_bayar" id="tgl_bayar" value="<?= $row[0]->tgl_bayar ?>">
        <label for="bulan_dibayar">bulan_dibayar</label>
        <input type="text" name="bulan_dibayar" id="bulan_dibayar" value="<?= $row[0]->bulan_dibayar ?>">
        <label for="thn_dibayar">thn_dibayar</label>
        <input type="number" name="thn_dibayar" id="thn_dibayar" value="<?= $row[0]->thn_dibayar ?>">
        <label for="id_spp">id_spp</label>
        <input type="number" name="id_spp" id="id_spp" value="<?= $row[0]->id_spp ?>">
        <label for="jumlah_dibayar">jumlah_dibayar</label>
        <input type="text" name="jumlah_dibayar" id="jumlah_dibayar" value="<?= $row[0]->jumlah_dibayar ?>">

        <input type="submit" value="Sunting">
    </form>
</body>

</html
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<style>
    table,
    th,
    td {
        border: 1px solid black;
    }
</style>

<body>
    <h1>Data Pembayaran</h1>
    <h6><a href="pembayaran/add/">Tambah Data Pembayaran</a></h6>
    <table>
        <thead>
            <tr>
                <td>id_pembayaran</td>
                <td>id_petugas</td>
                <td>nisn</td>
                <td>tgl_bayar</td>
                <td>bulan_dibayar</td>
                <td>thn_bayar</td>
                <td>id_spp</td>
                <td>jumlah_dibayar</td>
                <td>Aksi</td>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($query->result() as $row) { ?>
                <tr>
                    <td><?= $row->id_pembayaran ?></td>
                    <td><?= $row->id_petugas ?></td>
                    <td><?= $row->nisn ?></td>
                    <td><?= $row->tgl_bayar ?></td>
                    <td><?= $row->bulan_dibayar ?></td>
                    <td><?= $row->thn_dibayar ?></td>
                    <td><?= $row->id_spp ?></td>
                    <td><?= $row->jumlah_dibayar ?></td>
                    <td>
                        <a href="<?= base_url('pembayaran/edit/') . $row->id_pembayaran ?>">Edit</a>
                        <a href="<?= base_url('pembayaran/delete/') . $row->id_pembayaran ?>">Delete</a>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</body>

</html>
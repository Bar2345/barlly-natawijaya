<?php

class kelas_Model extends CI_Model
{
  public function create()
  {
    $data = array(
      'nama_kelas' => $this->input->post('nama_kelas'),
      'kompetensi_keahlian'=>$this->input->post('kompetensi_keahlian')
    );

    $this->db->insert('kelas', $data);
  }
  public function read()
  {
  
    $query = $this->db->get('kelas');
    return $query;
  }
  public function read_by_kelas($id_kelas)
  {
    $query = $this->db->get_where('kelas', array('id_kelas' => $id_kelas));
    return $query;
  }
  public function update()
  { {
      $data = array(
        'id_kelas' => $this->input->post('id_kelas'),
        'nama_kelas' => $this->input->post('nama_kelas'),
        'kompentensi_keahlian' => $this->input->post('kompentensi_keahlian')
      );
      $this->db->update('kelas', $data, array('id_kelas' => $this->input->post('id_kelas')));
    }  }
  public function delete($id_kelas)
  {
    $this->db->delete('kelas', array('id_kelas' => $id_kelas));  // Produces: // DELETE FROM mytable  // WHERE id = $id 
  }
}

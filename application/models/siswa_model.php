<?php

class siswa_Model extends CI_Model
{
  public function create()
  {
    $data = array(
      'nisn' => $this->input->post('nisn'),
      'nis'=>$this->input->post('nis'),
      'nama' => $this->input->post('nama'),
      'id_kelas' => $this->input->post('id_kelas'),
      'alamat' => $this->input->post('alamat'),
      'no_telp' => $this->input->post('no_telp'),
      'id_spp' => $this->input->post('id_spp'),
      'password' => $this->input->post('password')
    );

    $this->db->insert('siswa', $data);
  }
  public function read()
  {
    $query = $this->db->get('siswa');
    return $query;
  }
  public function read_by_nisn($nisn)
  {
    $query = $this->db->get_where('siswa', array('nisn' => $nisn));
    return $query;
  }
  public function update()
  {
    $data = array(
      'nisn' => $this->input->post('nisn'),
      'nis' => $this->input->post('nis'),
      'nama' => $this->input->post('nama'),
      'id_kelas' => $this->input->post('id_kelas'),
      'alamat' => $this->input->post('alamat'),
      'no_telp' => $this->input->post('no_telp'),
      'id_spp' => $this->input->post('id_spp'),
      'password' => $this->input->post('password')

    );
    $this->db->update('siswa', $data, array('nisn' => $this->input->post('nisn')));  
  }
  public function delete($nisn)
  {
    $this->db->delete('siswa', array('nisn' =>$nisn)); // Produces: // DELETE FROM mytable  // WHERE id = $id
  }
}

<?php

class Pembayaran_Model extends CI_Model
{
  public function create()
  {
    $data = array(
      'id_pembayaran' => $this->input->post('id_pembayaran'),
      'id_petugas'=>$this->input->post('id_petugas'),
      'nisn' => $this->input->post('nisn'),
      'tgl_bayar' => $this->input->post('tgl_bayar'),
      'bulan_dibayar' => $this->input->post('bulan_dibayar'),
      'thn_dibayar' => $this->input->post('thn_dibayar'),
      'id_spp' => $this->input->post('id_spp'),
      'jumlah_dibayar' => $this->input->post('jumlah_dibayar')
      
    );

    $this->db->insert('pembayaran', $data);
  }
  public function read()
  {
    $query = $this->db->get('pembayaran');
    return $query;
  }
  public function read_by_pembayaran($id_pembayaran)
  {
    $query = $this->db->get_where('pembayaran', array('id_pembayaran' => $id_pembayaran));
    return $query;
  }
  public function update()
  {
    $data = array(
      'id_pembayaran' => $this->input->post('id_pembayaran'),
      'id_petugas' => $this->input->post('id_petugas'),
      'nisn' => $this->input->post('nisn'),
      'tgl_bayar' => $this->input->post('tgl_bayar'),
      'bulan_dibayar' => $this->input->post('bulan_dibayar'),
      'thn_dibayar' => $this->input->post('thn_dibayar'),
      'id_spp' => $this->input->post('id_spp'),
      'jumlah_dibayar' => $this->input->post('jumlah_dibayar')
    );
    $this->db->update('pembayaran', $data, array('id_pembayaran' => $this->input->post('id_pembayaran')));  
  }
  public function delete($id_pembayaran)
  {
    $this->db->delete('pembayaran', array('id_pembayaran' => $id_pembayaran));  // Produces: // DELETE FROM mytable  // WHERE id = $id
  }

}

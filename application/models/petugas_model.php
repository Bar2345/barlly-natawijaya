<?php

class petugas_Model extends CI_Model
{
  public function create()
  {
    $data = array(
      
      'username' => $this->input->post('username'),
      'password'=>$this->input->post('password'),
      'nama_petugas' => $this->input->post('nama_petugas'),
      'level' => $this->input->post('level')
    );

    $this->db->insert('petugas', $data);
  }

  public function read()
  {
    $query = $this->db->get('petugas');
    return $query;
  }

   public function read_by_petugas($id_petugas)
  {
    $query = $this->db->get_where('petugas', array('id_petugas' => $id_petugas));
    return $query;
  }

  public function update()
  {
      $data = array(
      'username' => $this->input->post('username'),
      'password' => $this->input->post('password'),
      'nama_petugas' => $this->input->post('nama_petugas'),
      'level' => $this->input->post('level')
      );
    $this->db->update('petugas', $data, array('id_petugas' => $this->input->post('id_petugas')));  
  }

  public function delete($id_petugas)
  {
    $this->db->delete('petugas', array('id_petugas' => $id_petugas)); // Produces: // DELETE FROM mytable  // WHERE id = $id
  }
}